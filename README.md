# Bookshelf
app with hanami (hanamirb.org)

* Ruby version: 2.3.0

`gem install hanami`  

`hanami new bookshelf`

## how to run ?

`git clone git@bitbucket.org:acharya877/bookshelf.git`  

`set DATABASE_URL="postgres://<username>:<password>@localhost/bookshelf_development"` on `.env` file

setup db: `bundle exec hanami db create` , `HANAMI_ENV=test bundle exec hanami db create`

migrate: `bundle exec hanami db migrate` , `HANAMI_ENV=test bundle exec hanami db migrate`

`bundle exec hanami server`


## test

`rake test`  


